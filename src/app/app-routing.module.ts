import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { single } from 'rxjs';
import { HomeComponent } from './home/home.component';
import { OffenceComponent } from './offence/offence.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'contact', component: OffenceComponent },
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
